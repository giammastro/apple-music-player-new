//
//  Songs.swift
//  Giammarco's Ultimate Apple Music Search Screen Simulator 2019 (TM)
//
//  Created by Giammarco Mastronardi on 11/12/2019.
//  Copyright © 2019 Giammarco Mastronardi. All rights reserved.
//

import Foundation
import UIKit

class Song {
    var image: UIImage
    var title: String
    var artist: String
//    let categoria : [String]
    
    init(image: UIImage, title: String, artist: String) {
        self.image = image
        self.title = title
        self.artist = artist
    }
    
}
