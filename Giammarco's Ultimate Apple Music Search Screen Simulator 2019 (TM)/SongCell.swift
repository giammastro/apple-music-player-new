//
//  SongCell.swift
//  Giammarco's Ultimate Apple Music Search Screen Simulator 2019 (TM)
//
//  Created by Giammarco Mastronardi on 11/12/2019.
//  Copyright © 2019 Giammarco Mastronardi. All rights reserved.
//

import UIKit

class SongCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBOutlet weak var SongImage: UIImageView!
    @IBOutlet weak var Title: UILabel!
    @IBOutlet weak var Artist: UILabel!
    
    func setSong(song: Song) {
        SongImage.image = song.image
        Title.text = song.title
        Artist.text = song.artist
    }
    
}
